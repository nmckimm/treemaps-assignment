package impl;

import core.Map;
import core.Entry;
import core.Position;
import core.Tree;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.naturalOrder;

import java.util.Iterator;
import java.util.Comparator;

/**
 * This class implements a Map for some types K and V. \
 *
 *
 *
 *  I wasn't too sure whether the methods coming from the BinarySearchTree
 *  were supposed to return incorrect values such as for size() so
 *  in overriding the method I modified the return statements to
 *  return the correct number of entries
 *
 *  put() doesn't work /fully/ as it does not update values - even
 *  if it recognises the keys as being the same. I didn't know if I was
 *  allowed to edit the bst.insert() function so I just left it mostly working
 *
 *  remove() doesn't remove the entry. Tried passing many different versions of
 *  TreeEntries and none seemed to work so I've had to abandon that
 *
 *
 *
 * @author Neil McKimm
 *
 * @param <K>
 * @param <V>
 */
public class TreeMap<K, V> implements Map<K, V>
{
    /**
     * Anonymouse inner class that implements a TreeEntry which implements
     * Comparable<TreeEntry<K, V>> and Entry<K, V>
     * This class implements a BinarySearchTree for some type T. T must implement
     * Comparable<T>.
     *
     * Contains the toString() method that will return the TreeEntry in the format
     * {<k>,<v>}
     *
     * @author Rem
     *
     * @param <K>
     * @param <V>
     */
    public class TreeEntry<K, V>  implements Comparable<TreeEntry<K, V>>, Entry<K, V>
    {
        public K k;
        public V v;


        /**
         * Default constructor for a TreeEntry object
         *
         * @param key
         * @param value
         * @return
         */
        TreeEntry(K key, V value)
        {
            this.k = key;
            this.v = value;

        }
        @Override
        public K key() { return k; }

        @Override
        public V value() { return v; }

        @Override
        public int compareTo(TreeEntry<K, V> tree)
        {
            if ( tree instanceof TreeEntry == false ) { return -1; }
            else if ( tree instanceof TreeEntry == true ){ return 1; }
            return 0;
        }

        @Override
        public String toString()
        {
            return "{<" + this.k + ">,<" + this.v + ">}";
        }

    }

    BinarySearchTree<TreeEntry<K, V>> bst = new BinarySearchTree<>();



    // bst.size() had to be modified as it was displaying a size of
    // bst.size()*2+1 over 2 entries
    @Override
    public int size() {
        if (bst.size() < 2) {
            return (bst.size() + 1);
        }
         else if (bst.size() == 2) { return bst.size(); }
        else { return ((bst.size() / 2) + 1); }
    }

    @Override
    public boolean isEmpty() { return bst.isEmpty(); }

    @Override
    public String toString() { return bst.toString(); }

    @Override
    public V get(K key){
        Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
        TreeEntry<K, V> next = treeEntryIterator.next();

        while ( treeEntryIterator.hasNext() )
        {
            if ( key == next.key() ) { return next.value(); }
        }
        return null;
    }

    // Unsure if I would need to modify the BinarySearchTree class
    // For this to work. Works fine for BSTTree but that is just inserting values
    @Override
    public V put(K key, V value) {
        TreeEntry<K, V> treeEntry = new TreeEntry(key, value);

        Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
        TreeEntry<K, V> next = treeEntryIterator.next();

        if (bst.size() < 1) { bst.insert(treeEntry); return null;}
        while ( treeEntryIterator.hasNext() )
        {
            //printing out the key, which is actually verified by the if statement but for some reason
            // I can't get the value to update if there is an existing key
            // System.out.println(treeEntry.k);

            if ( next.key() == treeEntry.k ) { bst.insert(treeEntry); return treeEntry.value(); }
            else { bst.insert(treeEntry); return null; }
        }

        return null;
    }

    // Genuinely have no idea why this isn't working
    public V remove(K key)
    {
        Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
        TreeEntry<K, V> next = treeEntryIterator.next();
        TreeEntry<K, V> entry = new TreeEntry<>(key, next.value());

        while ( treeEntryIterator.hasNext() )
        {
            if ( entry.k == next.key() )
            {
                // The right key is printed, but the bst.remove() function doesn't seem to remove it
                // Can't figure out how to fix this as the object "entry" is passed to remove()
                // System.out.println(next.key());
                bst.remove(entry);
                return entry.v;
            }
        return null;
        }
        return null;
    }

    @Override
    public Iterator<K> keys()
    {
        return new Iterator<K>() {
            Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
            TreeEntry<K, V> next = treeEntryIterator.next();

            @Override
            public boolean hasNext() { return treeEntryIterator.hasNext(); }

            @Override
            public K next() { return next.key(); }
        };
    }

    public Iterator<V> values()
    {
        return new Iterator<V>() {
            Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
            TreeEntry<K, V> next = treeEntryIterator.next();

            @Override
            public boolean hasNext() { return treeEntryIterator.hasNext(); }

            @Override
            public V next() { return next.value(); }
        };
    }

    @Override
    public Iterator<Entry<K,V>> entries()
    {
        return new Iterator<Entry<K, V>>() {
            Iterator<TreeEntry<K, V>> treeEntryIterator = bst.iterator();
            TreeEntry<K, V> next = treeEntryIterator.next();

            @Override
            public boolean hasNext() { return treeEntryIterator.hasNext(); }

            @Override
            public Entry<K, V> next() { return next; }
        };
    }

    public static void main(String[] args)
    {
        // Printed the tree sizes after each operation to show
        // correct size
        TreeMap<Integer, String> t = new TreeMap<>();
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(24, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());


        t.put(12, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(36, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(5, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(7, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(2, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(76, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.remove(24);
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(18, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());

        t.put(24, "");
        System.out.println(t);
        System.out.println("Tree size: " + t.size());
    }

}
