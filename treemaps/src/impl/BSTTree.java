package impl;
import impl.BinarySearchTree;
public class BSTTree {

        public static void main(String[] args)
        {
            BinarySearchTree<Integer> test = new BinarySearchTree<Integer>();

            test.insert(24);
            System.out.println(test + "====================");
            test.insert(12);
            System.out.println(test + "====================");
            test.insert(36);
            System.out.println(test + "====================");
            test.insert(5);
            System.out.println(test + "====================");
            test.insert(7);
            System.out.println(test + "====================");
            test.insert(2);
            System.out.println(test + "====================");
            test.insert(76);
            System.out.println(test + "====================");
            test.remove(24);
            System.out.println(test + "====================");
            test.insert(18);
            System.out.println(test + "====================");
            test.insert(24);
            System.out.println(test + "====================");
        }
    }


